# install.packages("MASS")
library(MASS)
# color
grp_col <- c("#ADADAD","#FF79BC", "red", "black")
# path
args <- "/Users/shihhsin/Dropbox/Lab (JAC Lab)/Project- miR-34;449/Contour plot/Data for contour plotting"
save_path <- "/Users/shihhsin/Dropbox/Lab (JAC Lab)/Project- miR-34;449/Contour plot/Contour plots"
# x, y range
x_cor <- c(0, 1000)
y_cor <- c(-750, 750)


dir_lists <- list.dirs(args)[-1]

readFileFun <-  function(x){
  filelists <- list.files(x)
  raw_data <-lapply(paste(x, filelists, sep ="/"), FUN = read.csv, header = F)
  grp_data <- c()
  for (i in c(1:(length(raw_data)/2))) {
    tmp <- cbind(as.numeric(raw_data[[i*2]][4:dim(raw_data[[i*2]])[1], 1]) - as.numeric(raw_data[[i*2-1]][4, 1]), 
                 as.numeric(raw_data[[i*2]][4:dim(raw_data[[i*2]])[1], 2]) - as.numeric(raw_data[[i*2-1]][4, 2]), 
                 # sub(paste(args, "/", sep = ""), replacement = "", x)
                 strsplit(x, split = "/")[[1]][length(strsplit(x, split = "/")[[1]])]
                 )
    grp_data <- rbind(grp_data, tmp)
  }
  return(grp_data)
}

file_data <- lapply(dir_lists, FUN = readFileFun)
names(file_data) <- dir(args)
pull_data <- as.data.frame(do.call(rbind, file_data[1:length(dir(args))]))
colnames(pull_data) <- c("Position.X", "Position.Y","grp")

########  dot contour and marginal distribution
dot_line_fun <- function(t){
  pdf(paste(paste(save_path, t, sep = "/"), ".pdf", sep = ""))
  x <- as.numeric(file_data[[t]][, 1])
  y <- as.numeric(file_data[[t]][, 2])
  z <- kde2d(x, y, n = 100)
  zones <- matrix(c(2,0,1,3), ncol=2, byrow=TRUE)
  layout(zones, widths=c(4/5,1/5), heights=c(1/5,4/5))
  par(mar=c(8,8,1,1))
  plot(x, y, col = "#ADADAD", box(which = "plot", lwd =2), xlab = "Position.x", ylab = "Position.Y", cex.lab = 1.5, pch =16, cex =1, cex.axis = 1.5, xlim = x_cor, ylim = y_cor)
  contour(z, drawlabels = F, add=TRUE, lwd = 2, col = "blue", 
          # nlevels = 6,
          levels = seq(min(z$z), max(z$z), (max(z$z) - min(z$z))/11)[5:11]
  )
  par(mar=c(0,8,1,1))
  plot(density(x)$x, density(x)$y, axes=FALSE , type = "l", lwd = 2, col = "red", xlim = x_cor, ylab = "")
  par(mar=c(8,0,1,1))
  plot(density(y)$y, density(y)$x, axes=FALSE, type = "l", lwd = 2, col = "red", ylim = y_cor, xlab = "")
  dev.off()
}
lapply(dir(args), FUN = dot_line_fun)

########## scatter and marginal distribution plot

pdf(paste(save_path, "scatter.pdf", sep = "/"))
attach(pull_data)

zones <- matrix(c(2,4,1,3), ncol=2, byrow=TRUE)
layout(zones, widths=c(4/5,1/5), heights=c(1/5,4/5))
par(mar=c(8,8,1,1))
plot(Position.X, Position.Y, xlab = "Position.x", ylab = "Position.Y", cex.lab = 1.5, pch =16, cex =1.5, cex.axis = 1.5, xlim = x_cor, ylim = y_cor, type= "n")
box(which = "plot", lwd =2)
for (i in c(1:length(dir(args)))) {
  points(Position.X[grp == dir(args)[i]], Position.Y[grp == dir(args)[i]], pch =16, cex =0.5, cex.lab = 1.5, col = grp_col[i])
}

par(mar=c(0,8,1,1))
den_X_data <- c()
for (i in c(1:length(dir(args)))) {
  tmp <- cbind(density(as.numeric(file_data[[i]][,1]))$x, density(as.numeric(file_data[[i]][,1]))$y, rep(dir(args)[i], length(density(as.numeric(file_data[[i]][,1]))$x)))
  den_X_data <- rbind(den_X_data, tmp)
}
den_X_data <-as.data.frame(den_X_data)
plot(den_X_data$V1, den_X_data$V2, axes=FALSE , type = "n", xlim = x_cor, ylab = "")
for (i in c(1:length(dir(args)))) {
  lines(density(as.numeric(file_data[[i]][,1]))$x, density(as.numeric(file_data[[i]][,1]))$y, lwd = 2, col = grp_col[i])
}

par(mar=c(8,0,1,1))
den_Y_data <- c()
for (i in c(1:length(dir(args)))) {
  tmp <- cbind(density(as.numeric(file_data[[i]][,2]))$x, density(as.numeric(file_data[[i]][,2]))$y, rep(dir(args)[i], length(density(as.numeric(file_data[[i]][,2]))$x)))
  den_Y_data <- as.data.frame(rbind(den_Y_data, tmp))
}
plot(den_Y_data$V2, den_Y_data$V1, axes=FALSE , type = "n", ylim = y_cor, xlab = "")
for (i in c(1:length(dir(args)))) {
  lines(density(as.numeric(file_data[[i]][,2]))$y, density(as.numeric(file_data[[i]][,2]))$x, lwd = 2, col = grp_col[i])
}

par(mar=c(3,0,1,1))
plot(Position.X, Position.Y, type = "n", bty = "n", xaxt = "n", yaxt = "n")
legend(0, 600, legend = dir(args), col = grp_col, box.lty = 0, lty = 1, cex=0.8, lwd =2)

dev.off()

#######  contour plot
pdf(paste(save_path, "contour.pdf", sep = "/"))
par(mar=c(5,5,3,3))
plot(Position.X, Position.Y, type = "n", xlab = "Position.x", ylab = "Position.Y", cex.lab = 1.5, cex.axis = 1.5, xlim = x_cor, ylim = y_cor)
box(which = "plot", lwd =2)
for (i in c(1:length(dir(args)))) {
  z_con <- kde2d(as.numeric(Position.X[grp == dir(args)[i]]), as.numeric(Position.Y[grp == dir(args)[i]]), n = 50)
  contour(z_con, drawlabels=FALSE, nlevels=6,  add=TRUE, lwd = 2, col = grp_col[i],
          levels = seq(min(z_con$z), max(z_con$z), (max(z_con$z) - min(z_con$z))/11)[5:11]
          )
}
dev.off()
   